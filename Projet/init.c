#include<stdint.h>

extern uint8_t _bstart,_bend,_data,_edata,_text_begin,_etext,_irqbegin,_irqend;
extern uint8_t _lma_text_start, _lma_data_start,_lma_irq_start;


//On initialise le BSS a zero. Cette code doit etre dans la Flash!
__attribute__((section(".textROM"))) void init_bss()  {
    uint8_t *dst;
    for (dst = &_bstart; dst < &_bend; dst++) {
        *dst = 0;
    }
}
// On fait une copie de le segment DATA en Flash a une section DATA en RAM; Cette code doit etre dans la Flash!
__attribute__((section(".textROM"))) void copy_data() {
    uint8_t *ptr_memr;                                                       // La logique est la meme pour les trois cas, donc je vais detailer ici seulement. On a crée un pointeur "memory"
    uint8_t *ptr_fl = &_lma_data_start;                                      // on a un autre pointeur qui pointe vers le debut de le data dans la flash
    for(ptr_memr = &_data; ptr_memr < &_edata; ptr_memr++) {                 // on fait le pointeur memory commencer a l'address de debut de data dans la RAM et incrememente jusqu'a la fin.
        *ptr_memr = *ptr_fl;                                                 // on fait tout égal = une copie!
         ptr_fl++;                                                           // on incremente le pointeur vers la RAM pour faire des bonnes copies
    }
}
// On fait une copie de le segment TEXT en Flash a une section TEXT en RAM; Cette code doit etre dans la Flash!
__attribute__((section(".textROM"))) void copy_text() {
    uint8_t *ptr_mem;                                       
    uint8_t *ptr_flash = &_lma_text_start;
    for(ptr_mem = &_text_begin; ptr_mem < &_etext; ptr_mem++) {
        *ptr_mem = *ptr_flash;
         ptr_flash++;
    }

}
// On fait une copie de le table IRQ!
__attribute__ ((section(".textROM"))) void copy_irq() {
    uint8_t *ptr_ram;
    uint8_t *ptr_rom = &_lma_irq_start;
    for(ptr_ram = &_irqbegin; ptr_ram < &_irqend; ptr_ram ++) {
        *ptr_ram = *ptr_rom;
        ptr_rom++;
    }

}
__attribute__ ((section(".textROM"))) void init_tout() {
    copy_text();
    copy_data();
    init_bss();
    copy_irq();
}