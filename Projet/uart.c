#include "stm32l475xx.h"
#include <stdint.h>
#include <stddef.h>
#include "matrix.h"



void uart_init(int baudrate){
    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;                                                                                     // Allumer l'horloge de la porte B 

    //Set up Registres

    GPIOB->MODER = (GPIOB->MODER & ~GPIO_MODER_MODE6_0)|(GPIO_MODER_MODE6_1);                                                 // Mettre en Alternate Function (porte 6)
    GPIOB->MODER = (GPIOB->MODER & ~GPIO_MODER_MODE7_0)|(GPIO_MODER_MODE7_1);                                                 // Mettre en Alternate Function (porte 7)
    GPIOB->AFR[0] = (GPIOB->AFR[0] & ~GPIO_AFRL_AFSEL6_Msk) | GPIO_AFRL_AFSEL6_2 |GPIO_AFRL_AFSEL6_1 | GPIO_AFRL_AFSEL6_0;   // Mettre AF7 dans le port 6 (TX)
    GPIOB->AFR[0] = (GPIOB->AFR[0] & ~GPIO_AFRL_AFSEL7_Msk) | GPIO_AFRL_AFSEL7_2 |GPIO_AFRL_AFSEL7_1 | GPIO_AFRL_AFSEL7_0;   // Mettre AF7 dans le port 7 (RX)
    RCC->APB2ENR |= RCC_APB2ENR_USART1EN_Msk;                                                                                 // Activer l'horloge USART1
    RCC->CCIPR = RCC->CCIPR & ~RCC_CCIPR_USART1SEL;                                                                           // Selectioner PCLK
    
    // Faire le Reset (Precaution)
    RCC->APB2RSTR |= RCC_APB2RSTR_USART1RST;                                         // On met le bit reset = 1 (reset state on)
    RCC->APB2RSTR = RCC->APB2RSTR & ~RCC_APB2RSTR_USART1RST;                         // On met le bit reset = 0 (reset state off)

    USART1->CR1 = 0;
    // Configurer la vitesse du port serie
    int speed = 80000000/baudrate;
    USART1->BRR = speed;

    // Configurer l'oversampling 
    USART1->CR1 = 0;                                                                 // avec ce bit a zero = pas de bit de parité
    USART1->CR2 = 0;                                                                 // Avec 00 = un Stop Bit

    // Activer l'USART
    USART1->CR1 |= USART_CR1_UE | USART_CR1_RE | USART_CR1_TE | USART_CR1_RXNEIE;      //On allume le porte, le receiver, le transmitter, et l'interruption

    NVIC_EnableIRQ(37);                                                                // On habilite l'interruption

}

void uart_putchar(uint8_t c) {
    while(!(USART1->ISR & USART_ISR_TXE));                                            //on attend jusqu'au moment ou le TXE est 1
    USART1->TDR = c;                                                                  //on ecrit dans le registre de transmission
}

uint8_t uart_getchar() {
    while(!(USART1->ISR & USART_ISR_RXNE));                                                //on attend jusqu'au moment ou le RXNE est 1
    return (uint8_t) USART1->RDR;                                                          //on retourne ce que est dans le registre de reception
}

void uart_puts(const uint8_t *str) {
    uint32_t n;
    for(n = 0; str[n] != '\0'; n++) {
        uart_putchar(str[n]);
    }
    uart_putchar(0xd);
    uart_putchar('\n');
}

void uart_gets(uint8_t *str, size_t size){
    size_t j = 0;
    uint8_t c;
    c = uart_getchar();
    while(j != size - 1 && c != (uint8_t) '\n' && c != 0xd){
        str[j] = c;
        j++;
        c = uart_getchar();
    }
    str[j] = '\0';
}

uint32_t sum(int number_of_data) {                                                  // on va dire en avance combien de numeros qu'on va passer comme argument
    int i;
    uint32_t summation = 0;
    for(i = 0; i < number_of_data; i ++) {
        summation = summation + uart_getchar();
    }
    return summation;

}

