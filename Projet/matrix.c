#include "stm32l475xx.h"
#include "matrix.h"

void matrix_init() {
// PORT A, B et C HORLOGE ACTIVATION
     RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
     RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;                           // On allume le horloge pour les portes A, B et C
     RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN; 
    
// TOUTES LES PORTS EN OUTPUT MODE
    // Portes de C
    GPIOC->MODER = (GPIOC->MODER & ~GPIO_MODER_MODE3_Msk & ~GPIO_MODER_MODE4_Msk & ~GPIO_MODER_MODE5_Msk) |(GPIO_MODER_MODE3_0 | GPIO_MODER_MODE4_0 | GPIO_MODER_MODE5_0);
    // Portes de B
    GPIOB->MODER = (GPIOB->MODER & ~GPIO_MODER_MODE1_Msk & ~GPIO_MODER_MODE2_Msk & ~GPIO_MODER_MODE0_Msk) |(GPIO_MODER_MODE1_0 | GPIO_MODER_MODE2_0 | GPIO_MODER_MODE0_0);
    // Portes de A (C'est pas très lisible je sais, mais cela altere tous les bits d'un coup quand meme)
    GPIOA->MODER = (GPIOA->MODER & ~GPIO_MODER_MODE2_Msk & ~GPIO_MODER_MODE3_Msk & ~GPIO_MODER_MODE4_Msk & ~GPIO_MODER_MODE5_Msk & ~GPIO_MODER_MODE6_Msk & ~GPIO_MODER_MODE7_Msk & ~GPIO_MODER_MODE15_Msk) |(GPIO_MODER_MODE2_0 | GPIO_MODER_MODE3_0 | GPIO_MODER_MODE4_0| GPIO_MODER_MODE5_0 | GPIO_MODER_MODE6_0 | GPIO_MODER_MODE7_0 | GPIO_MODER_MODE15_0);



// TOUTES LES PORTS EN HAUTE VITESSE
    // Portes de C
    GPIOC->OSPEEDR = (GPIOC->OSPEEDR & ~GPIO_OSPEEDR_OSPEED3_Msk & ~GPIO_OSPEEDR_OSPEED4_Msk & ~GPIO_OSPEEDR_OSPEED5_Msk) | (GPIO_OSPEEDR_OSPEED3_1 |GPIO_OSPEEDR_OSPEED4_1 |GPIO_OSPEEDR_OSPEED5_1 );
    // Portes de B
    GPIOB->OSPEEDR = (GPIOB->OSPEEDR & ~GPIO_OSPEEDR_OSPEED0_Msk & ~GPIO_OSPEEDR_OSPEED1_Msk & ~GPIO_OSPEEDR_OSPEED2_Msk) | (GPIO_OSPEEDR_OSPEED0_1 |GPIO_OSPEEDR_OSPEED1_1 |GPIO_OSPEEDR_OSPEED2_1 );
    // Portes de A (De nouveau pas très lisible mais cela fonctionne)
    GPIOA->OSPEEDR = (GPIOA->OSPEEDR & ~GPIO_OSPEEDR_OSPEED2_Msk & ~GPIO_OSPEEDR_OSPEED3_Msk & ~GPIO_OSPEEDR_OSPEED4_Msk & ~GPIO_OSPEEDR_OSPEED5_Msk & ~GPIO_OSPEEDR_OSPEED6_Msk & ~GPIO_OSPEEDR_OSPEED7_Msk & ~GPIO_OSPEEDR_OSPEED15_Msk) | (GPIO_OSPEEDR_OSPEED2_1 |GPIO_OSPEEDR_OSPEED3_1 |GPIO_OSPEEDR_OSPEED4_1 | GPIO_OSPEEDR_OSPEED5_1 |GPIO_OSPEEDR_OSPEED6_1 |GPIO_OSPEEDR_OSPEED7_1 | GPIO_OSPEEDR_OSPEED15_1);

// METTRE LES BONNES VALEURS DANS LES REGISTRES
    // Portes B
    GPIOB->BSRR = GPIO_BSRR_BR0 | GPIO_BSRR_BR1 | GPIO_BSRR_BR2;                                                                             //PB0 = C6 = 0 | PB1 = SCK = 0 | PB2 = C0 = 0

    // Portes A
    GPIOA->BSRR = GPIO_BSRR_BR2 | GPIO_BSRR_BR3 | GPIO_BSRR_BR4 | GPIO_BSRR_BR5 | GPIO_BSRR_BR6 | GPIO_BSRR_BR7 | GPIO_BSRR_BR15;           // PA2 = C2 = 0 | PA3 = C7 = 0 | PA4 = SDA = 0 | PA5 = C5 = 0 | PA6 = C4 = 0 | PA7 = C3 = 0 | PA15 = C1 = 0   

    // Portes C
    GPIOC->BSRR = GPIO_BSRR_BR3 | GPIO_BSRR_BS4 | GPIO_BSRR_BS5;                                                                            //  PC3 = RST = 0 | PC4 = LAT = 1 | PC5 = SB = 1

// ATTENDRE 100ms ET METTRE LE RESET A 1. APRES, DESALLUMER
    for (int i=0; i< 80000 ; i++) {                                    //80000 * clock_period = 100ms
        asm volatile("nop");
    }
    GPIOC->BSRR = GPIOC->BSRR =(GPIOC->BSRR & ~GPIO_BSRR_BR3_Msk)|GPIO_BSRR_BS3;
    GPIOC->BSRR = (GPIOC->BSRR & ~GPIO_BSRR_BR3_Msk);                                // Desallumer le reset. On ne change pas les autres valeurs du registre!

    //INITIALISER LE BANK0
    init_bank0();      
}


// DEFINITIONS 

void deactivate_rows() {
    ROW0(0);
    ROW1(0);
    ROW2(0);
    ROW3(0);
    ROW4(0);
    ROW5(0);
    ROW6(0);
    ROW7(0);
}

void activate_row(int row) {
    if(row == 0) {
        ROW0(1);
    } else if (row == 1) {
        ROW1(1);
    } else if (row == 2) {
        ROW2(1);
    } else if (row == 3) {
        ROW3(1);
    } else if (row == 4) {
        ROW4(1);
    } else if (row == 5) {
        ROW5(1);
    } else if (row == 6) {
        ROW6(1);
    } else if (row == 7) {
        ROW7(1);
    }
}

void send_byte(uint8_t value, int bank) {
    if(bank == 0) {
        SB(0);
    } else {
        SB(1);
    }
    for(int i = 0; i < 8; i++) {
        if(value & 0x80) {
            SDA(1);
        } else {
            SDA(0);
        }
        value = value << 1;
        pulse_SCK;
    }
    
}
//cette fonction met tous les valeurs de bank0 a zero.
void init_bank0() {
    SB(0);
    SDA(1);
    for(int i = 0; i < 145; i++){                                      //on mantien le valeur 0 e lance beaucoup de pulses SCK pour remplir le bank0 avec zeros
        pulse_SCK;
    }    
    pulse_LAT;
}

//cette fonction allume une ligne
void mat_set_row(int row, const rgb_color *val) {
    //une for pour chaque element du tableau
    for(int i = 7; i >= 0; i--){
        send_byte(val[i].b,1);
        send_byte(val[i].g,1);
        send_byte(val[i].r,1);
    }
    deactivate_rows();
    for(int i = 0; i < 200; i++) {asm volatile ("nop");}                              //ce delay la c'est pour ne par avoir de repetion de l'image dans la derniere colomne
    pulse_LAT;
    activate_row(row);
}

// Cette fonction la c'est un test simple d'une image statique
void test_pixels() {
    rgb_color val [8];
    while(1) {
    for (int j = 0; j < 8; j = j + 3) {
        for (int i = 0; i < 8; i++) {                          //La premiere chose qu'on fait c'est initialiser le vecteur comme blue
            val[i].g = 0;
            val[i].b = 0xFF - (i)*0x20;   
            val[i].r = 0;
        }
        mat_set_row(j,val);
        for (int i = 0; i < 8; i++) {                          //La premiere chose qu'on fait c'est initialiser le vecteur comme vert
            val[i].b = 0;
            val[i].g = 0xFF - (i)*0x24;   
            val[i].r = 0;
        }
        mat_set_row(j+1,val);
        for (int i = 0; i < 8; i++) {                          //La premiere chose qu'on fait c'est initialiser le vecteur comme rouge
            val[i].b = 0;
            val[i].r = 0xFF - (i)*0x24;   
            val[i].g = 0;
        }
        mat_set_row(j+2,val);
    }
    }
}
// cette fonction c'est pour metre une matrix entiere sur le matrix de LEDs
void put_matrix(rgb_color *valmat) {
    for(int j = 0; j < 8; j++) {
        mat_set_row(j,valmat);
        valmat = valmat + 8;
    }
}  
