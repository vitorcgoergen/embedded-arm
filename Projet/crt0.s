    .syntax unified
    .global _start
    .thumb
    .section .textROM, "ax", %progbits

.thumb_func
_start:
    ldr sp, =_stack
    bl init_tout
    bl main

_exit:
    bl _exit
