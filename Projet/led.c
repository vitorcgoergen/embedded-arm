#include <stdint.h>
#include "led.h"
#include "stm32l475xx.h"



void led_init(){

    // PORT B et C HORLOGE ACTIVATION
     RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;                           // On allume le horloge pour le premier LED
     RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;                                                              
    
    
    // PORT B OUTPUT MODE                                                            Le but ici c'est mettre le port B en output mode.
    GPIOB->MODER = (GPIOB->MODER & ~GPIO_MODER_MODE14_Msk)|(GPIO_MODER_MODE14_0);        
    
    //PORT C                                                                         Ici, le port C
    GPIOC->MODER = (GPIOC->MODER & ~GPIO_MODER_MODE9_Msk)|(GPIO_MODER_MODE9_0);
    //DESALLUMER LES DEUX LEDS
    GPIOC->MODER = GPIOC->MODER | GPIO_MODER_MODE9_Msk;     //On mettre PC9 a mode input pour desallumer les deux leds
}

void led_g_on(){
    GPIOB->BSRR = GPIO_BSRR_BS14;
}
void led_g_off(){
    GPIOB->BSRR = GPIO_BSRR_BR14;
}

//Cette fonction c'est pour allumer un LED en accord avec l'input (led choisi)
void led(STATE state){
    switch(state){
        case LED_OFF:
            GPIOC->MODER = GPIOC->MODER | GPIO_MODER_MODE9_Msk;                   //On mettre PC9 a mode input pour desallumer les deux leds
            break;  
        case LED_YELLOW:
            GPIOC->MODER = (GPIOC->MODER & ~GPIO_MODER_MODE9_Msk)|(GPIO_MODER_MODE9_0);        //on mettre le led blue mode output
            GPIOC->BSRR = GPIO_BSRR_BS9;                                                       //on allume le led jaune (c'est la meme logique pour le LED bleu)
            break;
        case LED_BLUE:
            GPIOC->MODER = (GPIOC->MODER & ~GPIO_MODER_MODE9_Msk)|(GPIO_MODER_MODE9_0); 
            GPIOC->BSRR = GPIO_BSRR_BR9;
            break;
        default:
            break;
    }
}

