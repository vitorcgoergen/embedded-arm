#ifndef IRQ_H
#define IRQ_H
#include "stm32l475xx.h"

#define MAKE_DEFAULT_HANDLER(HANDLER_NAME) \
     void __attribute__((weak)) HANDLER_NAME(void) { \
        __disable_irq(); \
        while(1) {}; \
     }

void irq_init(void);
#endif