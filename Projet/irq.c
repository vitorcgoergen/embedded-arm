#include "stm32l475xx.h"
#include "irq.h"
#include "core_cm4.h"

MAKE_DEFAULT_HANDLER(NMI_Handler)
MAKE_DEFAULT_HANDLER(HardFault_Handler)
MAKE_DEFAULT_HANDLER(MemManage_Handler)
MAKE_DEFAULT_HANDLER(BusFault_Handler)
MAKE_DEFAULT_HANDLER(UsageFault_Handler)
MAKE_DEFAULT_HANDLER(SVC_Handler)
MAKE_DEFAULT_HANDLER(PendSV_Handler)
MAKE_DEFAULT_HANDLER(SysTick_Handler)
MAKE_DEFAULT_HANDLER(WWDG_IRQHandler)
MAKE_DEFAULT_HANDLER(PVD_PVM_IRQHandler)
MAKE_DEFAULT_HANDLER(TAMP_STAMP_IRQHandler)
MAKE_DEFAULT_HANDLER(RTC_WKUP_Handler)
MAKE_DEFAULT_HANDLER(FLASH_Handler)
MAKE_DEFAULT_HANDLER(RCC_Handler)
MAKE_DEFAULT_HANDLER(EXTI0_Handler)
MAKE_DEFAULT_HANDLER(EXTI1_Handler)
MAKE_DEFAULT_HANDLER(EXTI2_Handler)
MAKE_DEFAULT_HANDLER(EXTI3_Handler)
MAKE_DEFAULT_HANDLER(EXTI4_Handler)
MAKE_DEFAULT_HANDLER(DMA1_CH1_Handler)
MAKE_DEFAULT_HANDLER(DMA1_CH2_Handler)
MAKE_DEFAULT_HANDLER(DMA1_CH3_Handler)
MAKE_DEFAULT_HANDLER(DMA1_CH4_Handler)
MAKE_DEFAULT_HANDLER(DMA1_CH5_Handler)
MAKE_DEFAULT_HANDLER(DMA1_CH6_Handler)
MAKE_DEFAULT_HANDLER(DMA1_CH7_Handler)
MAKE_DEFAULT_HANDLER(ADC1_2_Handler)
MAKE_DEFAULT_HANDLER(CAN1_TX_Handler)
MAKE_DEFAULT_HANDLER(CAN1_RX0_Handler)
MAKE_DEFAULT_HANDLER(CAN1_RX1_Handler)
MAKE_DEFAULT_HANDLER(CAN1_SCE_Handler)
MAKE_DEFAULT_HANDLER(EXTI9_5_Handler)
MAKE_DEFAULT_HANDLER(TIM1_BRK_Handler)
MAKE_DEFAULT_HANDLER(TIM1_UP_Handler)
MAKE_DEFAULT_HANDLER(TIM_Handler)
MAKE_DEFAULT_HANDLER(TIM1_TRG_COM_Handler)
MAKE_DEFAULT_HANDLER(TIM1_CC_Handler)
MAKE_DEFAULT_HANDLER(TIM2_Handler)
MAKE_DEFAULT_HANDLER(TIM3_Handler)
MAKE_DEFAULT_HANDLER(TIM4_Handler)
MAKE_DEFAULT_HANDLER(I2C1_EV_Handler)
MAKE_DEFAULT_HANDLER(I2C1_ER_Handler)
MAKE_DEFAULT_HANDLER(I2C2_EV_Handler)
MAKE_DEFAULT_HANDLER(I2C2_ER_Handler)
MAKE_DEFAULT_HANDLER(SPI1_Handler)
MAKE_DEFAULT_HANDLER(SPI2_Handler)
MAKE_DEFAULT_HANDLER(USART1_Handler)
MAKE_DEFAULT_HANDLER(USART2_Handler)
MAKE_DEFAULT_HANDLER(USART3_Handler)
MAKE_DEFAULT_HANDLER(EXTI15_10_Handler)
MAKE_DEFAULT_HANDLER(RTC_ALARM_Handler)
MAKE_DEFAULT_HANDLER(DFSDM1_FLT3_Handler)
MAKE_DEFAULT_HANDLER(TIM8_BRK_Handler)
MAKE_DEFAULT_HANDLER(TIM8_UP_Handler)
MAKE_DEFAULT_HANDLER(TIM8_TRG_COM_Handler)
MAKE_DEFAULT_HANDLER(TIM8_CC_Handler)
MAKE_DEFAULT_HANDLER(ADC3_Handler)
MAKE_DEFAULT_HANDLER(FMC_Handler)
MAKE_DEFAULT_HANDLER(SDMMC1_Handler)
MAKE_DEFAULT_HANDLER(TIM5_Handler)
MAKE_DEFAULT_HANDLER(SPI3_Handler)
MAKE_DEFAULT_HANDLER(UART4_Handler)
MAKE_DEFAULT_HANDLER(UART5_Handler)
MAKE_DEFAULT_HANDLER(TIM6_DACUNDER_Handler)
MAKE_DEFAULT_HANDLER(TIM7_Handler)
MAKE_DEFAULT_HANDLER(DMA2_CH1_Handler)
MAKE_DEFAULT_HANDLER(DMA2_CH2_Handler)
MAKE_DEFAULT_HANDLER(DMA2_CH3_Handler)
MAKE_DEFAULT_HANDLER(DMA2_CH4_Handler)
MAKE_DEFAULT_HANDLER(DMA2_CH5_Handler)
MAKE_DEFAULT_HANDLER(DFSDM1_FLT0_Handler)
MAKE_DEFAULT_HANDLER(DFSDM1_FLT1_Handler)
MAKE_DEFAULT_HANDLER(DFSDM1_FLT2_Handler)
MAKE_DEFAULT_HANDLER(COMP_Handler)
MAKE_DEFAULT_HANDLER(LPTIM1_Handler)
MAKE_DEFAULT_HANDLER(LPTIM2_Handler)
MAKE_DEFAULT_HANDLER(OTG_FS_Handler)
MAKE_DEFAULT_HANDLER(DMA2_CH6_Handler)
MAKE_DEFAULT_HANDLER(DMA2_CH7_Handler)
MAKE_DEFAULT_HANDLER(LPUART1_Handler)
MAKE_DEFAULT_HANDLER(QUADSPI_Handler)
MAKE_DEFAULT_HANDLER(I2C3_EV_Handler)
MAKE_DEFAULT_HANDLER(I2C3_ER_Handler)
MAKE_DEFAULT_HANDLER(SAI1_Handler)
MAKE_DEFAULT_HANDLER(SAI2_Handler)
MAKE_DEFAULT_HANDLER(SWPMI1_Handler)
MAKE_DEFAULT_HANDLER(TSC_Handler)
MAKE_DEFAULT_HANDLER(LCD_Handler)
MAKE_DEFAULT_HANDLER(AES_Handler)
MAKE_DEFAULT_HANDLER(RNG_Handler)
MAKE_DEFAULT_HANDLER(FPU_Handler)



extern uint32_t _stack;
extern void _start(); 
// On va commencer avec le vecteur en ROM et après on fera une copie sur la RAM

void *vector_table[] __attribute__((aligned(512),section(".irq_table"))) = {
    // Stack and Reset Handler
    &_stack,            /* Top of stack */
    _start,             /* Reset handler */
    // ARM internal exceptions
    NMI_Handler,        /* NMI handler */
    HardFault_Handler,  /* Hard Fault handler */
    MemManage_Handler, 
    BusFault_Handler,
    UsageFault_Handler,
    0,                  /* Reserved */
    0,                  /* Reserved */
    0,                  /* Reserved */
    0,                  /* Reserved */
    SVC_Handler,        /* SVC handler */
    0,                  /* Reserved */
    0,                  /* Reserved */
    PendSV_Handler,     /* Pending SVC handler */
    SysTick_Handler,    /* SysTick hanlder */

    // STM32L475 External interrupts
    WWDG_IRQHandler,         /* Watchdog IRQ */
    PVD_PVM_IRQHandler,      /* PVD/PVM1/PVM2/PVM3/PVM4 through EXTI lines 16/35/36/37/38 interrupts */
    TAMP_STAMP_IRQHandler,   /*  RTC Tamper or TimeStamp /CSS on LSE through EXTI line 19 interrupts*/
    RTC_WKUP_Handler,                // RTC Wakeup Timer
    FLASH_Handler,                   // Flash Global Interrupt
    RCC_Handler,                     // RCC global interrupt
    EXTI0_Handler,                   // EXTIN = EXTI LineN interrupt
    EXTI1_Handler,
    EXTI2_Handler,
    EXTI3_Handler,
    EXTI4_Handler,
    DMA1_CH1_Handler,               //DMA1_CHN = DMA1_CHN DMA1 Channel N interrupt
    DMA1_CH2_Handler,
    DMA1_CH3_Handler,
    DMA1_CH4_Handler,
    DMA1_CH5_Handler,
    DMA1_CH6_Handler,
    DMA1_CH7_Handler,
    ADC1_2_Handler,                 // ADC1 & 2 global interrupt
    CAN1_TX_Handler,                // CAN1_XXX = CAN1_XXX Interrupt
    CAN1_RX0_Handler,               
    CAN1_RX1_Handler,
    CAN1_SCE_Handler,
    EXTI9_5_Handler,                //EXTI Line[9:5]
    TIM1_BRK_Handler,         //TIM1 Break/TIM15 global interrupt
    TIM1_UP_Handler,           //TIM1 Update/TIM16 global interrupt
    TIM1_TRG_COM_Handler,      //TIM1 Trigger and commutation
    TIM1_CC_Handler,
    TIM2_Handler,
    TIM3_Handler,
    TIM4_Handler,
    I2C1_EV_Handler,
    I2C1_ER_Handler,
    I2C2_EV_Handler,
    I2C2_ER_Handler,
    SPI1_Handler,
    SPI2_Handler,
    USART1_Handler,
    USART2_Handler,
    USART3_Handler,
    EXTI15_10_Handler,
    RTC_ALARM_Handler,
    DFSDM1_FLT3_Handler,
    TIM8_BRK_Handler,
    TIM8_UP_Handler,
    TIM8_TRG_COM_Handler,
    TIM8_CC_Handler,
    ADC3_Handler,
    FMC_Handler,
    SDMMC1_Handler,
    TIM5_Handler,
    SPI3_Handler,
    UART4_Handler,
    UART5_Handler,
    TIM6_DACUNDER_Handler,
    TIM7_Handler,
    DMA2_CH1_Handler,
    DMA2_CH2_Handler,
    DMA2_CH3_Handler,
    DMA2_CH4_Handler,
    DMA2_CH5_Handler,
    DFSDM1_FLT0_Handler,
    DFSDM1_FLT1_Handler,
    DFSDM1_FLT2_Handler,
    COMP_Handler,
    LPTIM1_Handler,
    LPTIM2_Handler,
    OTG_FS_Handler,
    DMA2_CH6_Handler,
    DMA2_CH7_Handler,
    LPUART1_Handler,
    QUADSPI_Handler,
    I2C3_EV_Handler,
    I2C3_ER_Handler,
    SAI1_Handler,
    SAI2_Handler,
    SWPMI1_Handler,
    TSC_Handler,
    LCD_Handler,
    AES_Handler,
    RNG_Handler,
    FPU_Handler,
};

void irq_init(void) {
    SCB->VTOR = (uint32_t)vector_table;
}

