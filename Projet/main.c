#include "fibo.h"
#include "led.h"
#include "clocks.h"
#include "uart.h"
#include "matrix.h"
#include <stdint.h>
#include <stddef.h>
#include "irq.h"
#include "button.h"
#include "stm32l475xx.h"

uint32_t summ = 0;
uint8_t color_vector[192];
uint8_t indext = 0;
uint8_t position = 0;
uint8_t column = 0;
uint8_t frame = 0;

void USART1_Handler();

int main() {
    //On fait tout les inits
    irq_init();
    clocks_init();
    led_init();
    button_init();
    uart_init(38400);
    matrix_init();
    extern uint8_t _binary_image_raw_start;                                                    //ces variables "viennent" du Makefile (cible image)
    uint8_t *ptr = &_binary_image_raw_start;                                                   //un pointeur au debut de l'image
    for(int i = 0; i < 192; i++) { 
        color_vector[i] = *ptr++;                                                              //on laisse l'image tout le temps
    }
    while(1) {
    put_matrix((rgb_color*) color_vector);
    }
    
}

void USART1_Handler() {
    uint8_t c = uart_getchar();
    position ++;
    if(c != 0xff) {
        if ((((USART1->ISR) & (USART_ISR_ORE_Msk)) == 0) && (((USART1->ISR) & (USART_ISR_FE_Msk)) == 0)) {                //on fait cela seulement si il n y a pas d'interruptions
        color_vector[indext] = c;
        indext ++;
        } else {
            USART1->ISR = USART1->ISR & (~USART_ISR_ORE_Msk & ~USART_ISR_FE_Msk);                                        //si il y a des errors on enleve les bits et attendre
        }

    } else {
        indext = 0;
        USART1->ICR = USART1->ICR | USART_ISR_ORE_Msk | USART_ISR_FE_Msk;                                            //on clear le tags d'erreur
    }

}
