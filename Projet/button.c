#include "button.h"
#include "stm32l4xx.h"
#include "led.h"

void button_init(void) {

    RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;                                                                     //Allumer le horloge de le C
    GPIOC->MODER = GPIOC->MODER & ~GPIO_MODER_MODE13_Msk;                                                     //Mettre la porte C comme Input
    SET_BIT(EXTI->IMR1,EXTI_IMR1_IM13_Msk);                                                                  //On configure le bit 13 pour mettre l'interrupt line habilité pour pouvoir generer l'interruption
    CLEAR_BIT(EXTI->RTSR1,EXTI_RTSR1_RT13_Msk);                                                              //Le bit de "upward" on mettre comme zero
    SET_BIT(EXTI->FTSR1,EXTI_FTSR1_FT13_Msk);                                                                //Le bit de "downward" on mettre comme 1
    NVIC_EnableIRQ(40);                                                                                      //NVIC_EnableIRQ(40)
    SYSCFG->EXTICR[3] = (SYSCFG->EXTICR[3] & ~SYSCFG_EXTICR4_EXTI13_Msk) | SYSCFG_EXTICR4_EXTI13_PC;         //On mettre le porte 13 comme source d'interruptions

}

void EXTI15_10_Handler() {
    static int st = 0;

    EXTI->PR1 |= EXTI_PR1_PIF13;
    if (st == 0) {
        led_g_on();                                                                                           //J'ai changé le handler seulement ici, a la derniere stage. Maintenant c'est un toggle, comme requis!
        st = 1;                                                                                               // le variable st c'est pour savoir si le LED est déjà allumé
    }
    else {   
        led_g_off();
        st = 0;
    }
}