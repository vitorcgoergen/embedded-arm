#ifndef MATRIX_H
#define MATRIX_H

//RGB COLOR
typedef struct {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} rgb_color;

//FONCTIONS
void matrix_init();
void deactivate_rows();
void activate_row(int row);
void send_byte(uint8_t value, int bank);
void init_bank0();
void mat_set_row(int row, const rgb_color *val);
void test_pixels();
void put_matrix(rgb_color *valmat);  




//MACROS
    //MACROS QUE UTILISENT LE REGISTRE C
        //RESET (PC3)
#define RST(X) do {if (X==0) GPIOC->BSRR = (GPIOC->BSRR & ~GPIO_BSRR_BS3) | GPIO_BSRR_BR3; else  GPIOC->BSRR =(GPIOC->BSRR & ~GPIO_BSRR_BR3_Msk)|GPIO_BSRR_BS3;} while(0);
        //SB (PC5)
#define SB(X) do {if (X==0) GPIOC->BSRR = (GPIOC->BSRR & ~GPIO_BSRR_BS5) | GPIO_BSRR_BR5; else  GPIOC->BSRR =(GPIOC->BSRR & ~GPIO_BSRR_BR5_Msk)|GPIO_BSRR_BS5;} while(0);
        //LAT (PC4)
#define LAT(X) do {if (X==0) GPIOC->BSRR = (GPIOC->BSRR & ~GPIO_BSRR_BS4) | GPIO_BSRR_BR4; else  GPIOC->BSRR =(GPIOC->BSRR & ~GPIO_BSRR_BR4_Msk)|GPIO_BSRR_BS4;} while(0);


    //MACROS QUE UTILISENT LE REGISTRE B
        //SCK (PB1)
#define SCK(X) do {if (X==0) GPIOB->BSRR = (GPIOB->BSRR & ~GPIO_BSRR_BS1) | GPIO_BSRR_BR1; else GPIOB->BSRR = (GPIOB->BSRR & ~GPIO_BSRR_BR1) | GPIO_BSRR_BS1 ;} while(0);            
        //ROW0 (PB2)
#define ROW0(X) do {if (X==0) GPIOB->BSRR = (GPIOB->BSRR & ~GPIO_BSRR_BS2) | GPIO_BSRR_BR2; else GPIOB->BSRR = (GPIOB->BSRR & ~GPIO_BSRR_BR2) | GPIO_BSRR_BS2 ;} while(0);
        //ROW6 (PB0)
#define ROW6(X) do {if (X==0) GPIOB->BSRR = (GPIOB->BSRR & ~GPIO_BSRR_BS0) | GPIO_BSRR_BR0; else GPIOB->BSRR = (GPIOB->BSRR & ~GPIO_BSRR_BR0) | GPIO_BSRR_BS0 ;} while(0);

    //MACROS QUI UTILISENT LE REGISTRE A
        //SDA (PA4)
#define SDA(X) do {if (X==0) GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BS4) | GPIO_BSRR_BR4; else GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BR4) | GPIO_BSRR_BS4 ;} while(0);
        //ROW1 (PA15)
#define ROW1(X) do {if (X==0) GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BS15) | GPIO_BSRR_BR15; else GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BR15) | GPIO_BSRR_BS15 ;} while(0);
        //ROW2 (PA2)
#define ROW2(X) do {if (X==0) GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BS2) | GPIO_BSRR_BR2; else GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BR2) | GPIO_BSRR_BS2 ;} while(0);
        //ROW3 (PA7)
#define ROW3(X) do {if (X==0) GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BS7) | GPIO_BSRR_BR7; else GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BR7) | GPIO_BSRR_BS7 ;} while(0);
        //ROW4 (PA6)
#define ROW4(X) do {if (X==0) GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BS6) | GPIO_BSRR_BR6; else GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BR6) | GPIO_BSRR_BS6 ;} while(0);
        //ROW5 (PA5)
#define ROW5(X) do {if (X==0) GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BS5) | GPIO_BSRR_BR5; else GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BR5) | GPIO_BSRR_BS5 ;} while(0);
        //ROW7 (PA3)
#define ROW7(X) do {if (X==0) GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BS3) | GPIO_BSRR_BR3; else GPIOA->BSRR = (GPIOA->BSRR & ~GPIO_BSRR_BR3) | GPIO_BSRR_BS3 ;} while(0);

    //MACROS POUR LES PULSES
#define pulse_SCK do {SCK(0); for(int i = 0; i < 3; i ++) {asm volatile("nop");} SCK(1); for(int i = 0; i < 3; i ++) {asm volatile("nop");} SCK(0);  for(int i = 0; i < 3; i ++) {asm volatile("nop");};} while (0);
#define pulse_LAT do {LAT(1); for(int i = 0; i < 3; i ++) {asm volatile("nop");} LAT(0); for(int i = 0; i < 3; i ++) {asm volatile("nop");} LAT(1);  for(int i = 0; i < 3; i ++) {asm volatile("nop");};} while (0);




#endif